Source: r-cran-rsample
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Andreas Tille <tille@debian.org>
Section: gnu-r
Testsuite: autopkgtest-pkg-r
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-cran-dplyr (>= 1.0.0),
               r-cran-furrr,
               r-cran-generics,
               r-cran-glue,
               r-cran-pillar,
               r-cran-purrr,
               r-cran-rlang (>= 0.4.10),
               r-cran-slider,
               r-cran-tibble,
               r-cran-tidyr,
               r-cran-tidyselect,
               r-cran-vctrs (>= 0.5.0)
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-cran-rsample
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-cran-rsample.git
Homepage: https://cran.r-project.org/package=rsample
Rules-Requires-Root: no

Package: r-cran-rsample
Architecture: all
Depends: ${R:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: GNU R general resampling infrastructure
 Classes and functions to create and summarize different types of
 resampling objects (e.g. bootstrap, cross-validation).
 .
 A resample is defined as the result of a two-way split of a data set.
 For example, when bootstrapping, one part of the resample is a sample
 with replacement of the original data. The other part of the split
 contains the instances that were not contained in the bootstrap sample.
 Cross-validation is another type of resampling.
